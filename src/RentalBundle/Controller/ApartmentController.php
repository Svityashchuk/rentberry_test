<?php

namespace RentalBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use RentalBundle\Entity\Apartment;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use RentalBundle\Form\ApartmentType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ApartmentController extends FOSRestController
{
    /**
     * List apartments
     *
     * @Rest\View
     */
    public function allAction()
    {
        $apartments = $this->getDoctrine()
            ->getRepository(Apartment::class)
            ->findAll();

        return ['apartments' => $apartments];
    }

    /**
     * Update action for apartment by it's id
     *
     * @Rest\View
     *
     * @param Apartment $apartment
     * @return ApartmentController|Response
     */
    public function editAction(Apartment $apartment)
    {
        return $this->processForm($apartment);
    }

    /**
     * Validate data from request, set data to model, persists model.
     * Service ExtendedDescriptionGenerator used for filling third field.
     *
     * @param Apartment $apartment
     * @return View|ApartmentController|Response
     */
    private function processForm(Apartment $apartment)
    {
        $request = Request::createFromGlobals();

        $form = $this->createForm(ApartmentType::class, $apartment);
        $form->submit(json_decode($request->getContent(), true), false);

        if ($form->isValid()) {
            $extendedDescription = $this
                ->get('rental.ext_descr_generator')
                ->getExtendedDescription($apartment);
            $apartment->setExtendedDescription($extendedDescription);

            $em = $this->getDoctrine()->getManager();
            $em->persist($apartment);
            $em->flush();

            $response = new Response();
            $response->setStatusCode(Response::HTTP_OK);

            return $response;
        }

        return View::create($form, Response::HTTP_BAD_REQUEST);
    }
}
