<?php
/**
 * Created by PhpStorm.
 * User: svityashchuk
 * Date: 30.08.17
 * Time: 11:50
 */

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use GuzzleHttp\Client;
use RentalBundle\Entity\Apartment;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class LoadApartmentData implements FixtureInterface
{
    private $linkAddress = 'https://rentberry.com/api/v1/apartment/search/';
    private $postData = [
        'googleId' => 'ChIJFx-NSo1ZwokRX4KvuDyAtLY',
        'url' => 'https://rentberry.com/apartments/s/new-york-ny',
        'zoom' => '14',
        'nwLatitude' => '40.72729020998645',
        'nwLongitude' => '-74.05636682948',
        'seLatitude' => '40.69827402874898',
        'seLongitude' => '-73.95551577052004',
    ];

    /**
     * Creating fixtures based on rentberry data from first page of New York city
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $client = new Client();
        $jsonEncoder = new JsonEncoder();

        $response = $client->request('POST', $this->linkAddress, [
            'form_params' => $this->postData
        ]);

        if ($response->getStatusCode() == 200) {
            $body = $response->getBody()->getContents();
            $decodedBody = $jsonEncoder->decode($body, 'json');

            foreach ($decodedBody['body']['apartments'] as $apartmentInfo) {
                $apartment = new Apartment();
                $apartment->setName($apartmentInfo['name']);
                $apartment->setDescription($this->prepareDescription($apartmentInfo));
                $apartment->setExtendedDescription(
                    implode(PHP_EOL, [$apartment->getName(), $apartment->getDescription()])
                );

                $manager->persist($apartment);
            }
            $manager->flush();
        }
    }

    private function prepareDescription(array $apartmentInfo) : string
    {
        return sprintf("%s: %s\n%s: %d\n",
            'Address', $apartmentInfo['formattedAddress'],
            'Price', $apartmentInfo['price']
        );
    }
}