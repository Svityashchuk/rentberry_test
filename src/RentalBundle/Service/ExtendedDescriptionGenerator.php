<?php
/**
 * Created by PhpStorm.
 * User: svityashchuk
 * Date: 30.08.17
 * Time: 17:15
 */

namespace RentalBundle\Service;

use RentalBundle\Entity\Apartment;

class ExtendedDescriptionGenerator
{
    /**
     * Generates string that contain concatenated string Name and Description of Apartment
     *
     * @param Apartment $apartment
     * @return string
     */
    public function getExtendedDescription(Apartment $apartment) : string
    {
        return implode(PHP_EOL, [$apartment->getName(), $apartment->getDescription()]);
    }
}